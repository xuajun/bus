<?php
/**
 * Created by PhpStorm.
 * User: xuajun
 * Date: 2021/2/1
 * Time: 17:28
 */

namespace Buerbus;

class Buerbus
{
    public function getInfo($id)
    {
        if ($id == 1) {
            return [
                'id' => '1',
                'phone' => '13812344321',
                'name' => '张三',
                'age' => '18',
                'sex' => '男',
            ];
        }
        return [];
    }
}
